const express = require('express');
const nanoid = require('nanoid');
const multer = require ('multer');
const path = require('path');
const config = require ('../config');


const createRouter = connection =>{

    const router = express.Router();


    const storage = multer.diskStorage({
        destination:  (req, file, cb)=> {
            cb(null, config.uploadPath);
        },
        filename:  (req, file, cb) => {
            cb(null, nanoid() + path.extname(file.originalname))
        }
    });


    const upload = multer({storage});

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `news`', (error, results)=>{
            if(error){
                return res.status(500).send({error:'Db not correct'})
            }
            return  res.send(results);
        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `news` WHERE `id`= ?',req.params.id, (error, results)=>{
            if(error){
                return  res.status(500).send({error:'Db not correct'})
            }
            if(results[0]){
                return  res.send(results[0]);
            }else{
                return  res.status(404).send({error:'News not found'})
            }

        });
    });

    router.delete('/:id',(req, res)=>{
        connection.query('DELETE  FROM `news` WHERE `id`=?',req.params.id, (error, results)=>{
            if(error){
                return res.status(500).send({error:'Db not correct'})
            } else {
                return  res.send({message: 'OK'})
            }
        })
    });

    router.post('/', upload.single('image'), (req, res) => {
        const news =req.body;
        console.log(news);

        news.date = new Date();

        if(req.file){
            news.image = req.file.filename;
        }
        connection.query('INSERT INTO `news`(`title`, `description`, `image`,`date`) VALUES (?,?,?,?)',
            [news.title, news.description, news.image,news.date],
            (error, results)=>{
                console.log(error);
                if(error){
                    return res.status(500).send({error:'Db not correct'})
                }
                return res.send({message: 'OK'});
            });
    });


    return router;
};


module.exports = createRouter;
