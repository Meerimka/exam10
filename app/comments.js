const express = require('express');


const createRouter = connection =>{
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `comments`', (error, results)=>{
            if(error){
                return res.status(500).send({error:'Db not correct'})
            }
            return res.send(results);
        });
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `comments` WHERE `news_id`= ?',req.params.id, (error, results)=>{
            if(error){
                return  res.status(500).send({error:'Db not correct'})
            }
            if(results[0]){
                return  res.send(results);
            }else{
                return  res.status(404).send({error:'Comment not found'})
            }

        });
    });


    router.delete('/:id',(req, res)=>{
        connection.query('DELETE  FROM `comments` WHERE `id`=?',req.params.id, (error, results)=>{
            if(error){
                return res.status(500).send({error:'Db not correct'})
            } else {
                return  res.send({message: 'OK'})
            }
        })
    });

    router.post('/', (req, res) => {
        const comment = req.body;
        if(req.body.author === '') {
           comment.author = 'Anonymous'
        }

        connection.query('INSERT INTO `comments` (`news_id`,`author`,`comments`) VALUES (?,?,?)',
            [comment.news_id,comment.author, comment.comments],
            (error,results)=>{
                if(error){
                    return res.status(500).send({error:'Db not correct'})
                }
                res.send({message: 'OK'});
            });

    });

    return router;

};



module.exports = createRouter;
