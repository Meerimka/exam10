CREATE SCHEMA `news-portal` DEFAULT CHARACTER SET utf8 ;

USE `news-portal`;

CREATE TABLE `news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `image` VARCHAR(255) NULL,
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `news_id` INT  NOT NULL,
  `author` VARCHAR(255) NULL,
  `comments` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_comments_id_idx` (`news_id` ASC),
  CONSTRAINT `fk_comments_id`
    FOREIGN KEY (`news_id`)
    REFERENCES `news` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

     INSERT INTO `news` (`id`,`title`,`description`,`date`)
    VALUES
    (1,'news1','some descriptions','2015-12-12'),
    (2,'news2','some descriptions','2015-12-12'),
    (3,'news3','some descriptions','2015-12-12');

     INSERT INTO `comments` (`id`,`news_id`,`author`,`comments`)
    VALUES
    (1,2,'user1','some comments'),
    (2,1,'user2','some comments'),
    (3,3,'user3','some comments');
